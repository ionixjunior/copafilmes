﻿using System.Collections.Generic;
using Api.Interfaces;
using Api.Models;
using Api.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Mobile.ViewModels;
using Moq;

namespace Mobile.UnitTest.ViewModels
{
    [TestClass]
    public class ResultViewModelTest
    {
        private Mock<IMovieService> _movieService;
        private Mock<ChampionshipService> _championshipService;
        private ResultViewModel _viewModel;

        [TestInitialize]
        public void Initialize()
        {
            var winners = new List<Result>();
            for (var i = 1; i <= 2; i++)
            {
                var result = new Result
                {
                    Position = i,
                    Player = new Movie()
                };

                winners.Add(result);
            }

            _movieService = new Mock<IMovieService>();

            _championshipService = new Mock<ChampionshipService>(_movieService.Object);
            _championshipService.Setup(e => e.GetResults())
                         .Returns(winners);

            _viewModel = new ResultViewModel(_championshipService.Object);
        }

        [TestCleanup]
        public void Cleanup()
        {
            _movieService = null;
            _championshipService = null;
            _viewModel = null;
        }

        [TestMethod]
        public void Title_Should_Be_Not_Null()
        {
            Assert.IsNotNull(_viewModel.Title);
        }

        [TestMethod]
        public void Results_Should_Be_Contains_Two_Players()
        {
            Assert.AreEqual(2, _viewModel.Results.Count);
        }
    }
}
