﻿using System.Linq;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Api.Models;
using Api.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Mobile.ViewModels;
using Moq;
using System.Collections.Generic;
using Api.Interfaces;
using Prism.Services;
using Prism.Navigation;

namespace Mobile.UnitTest.ViewModels
{
    [TestClass]
    public class PlayersViewModelTest
    {
        private Mock<IMovieService> _movieService;
        private ChampionshipService _championshipService;
        private Mock<IPageDialogService> _dialogService;
        private Mock<INavigationService> _navigationService;
        private PlayersViewModel _viewModel;

        [TestInitialize]
        public void Initialize()
        {
            var players = new List<Movie>();
            for (var i = 0; i < 16; i++)
                players.Add(new Movie { Id = i.ToString() });

            _movieService = new Mock<IMovieService>();
            _movieService.Setup(e => e.GetMoviesAsync())
                         .Returns(Task.FromResult<IList<Movie>>(players));

            _championshipService = new ChampionshipService(_movieService.Object);
            _dialogService = new Mock<IPageDialogService>();
            _navigationService = new Mock<INavigationService>();

            _viewModel = new PlayersViewModel(
                _championshipService, 
                _dialogService.Object, 
                _navigationService.Object
           );
        }

        [TestCleanup]
        public void Cleanup()
        {
            _movieService = null;
            _championshipService = null;
            _dialogService = null;
            _navigationService = null;
            _viewModel = null;
        }

        [TestMethod]
        public void Title_Should_Be_Not_Null()
        {
            Assert.IsNotNull(_viewModel.Title);
        }

        [TestMethod]
        public void Players_Should_Be_Null()
        {
            Assert.IsNotNull(_viewModel.Players);
        }

        [TestMethod]
        public void Players_Should_Be_Removed()
        {
            Assert.AreEqual(16, _viewModel.Players.Count);

            var player = _viewModel.Players.First();
            _viewModel.RemovePlayerCommand.Execute(player);

            Assert.AreEqual(15, _viewModel.Players.Count);
        }

        [TestMethod]
        public void Championship_Cant_Be_Started_At_Initialize()
        {
            Assert.IsFalse(_viewModel.StartChampionshipCommand.CanExecute(null));
        }

        [TestMethod]
        public void Championship_Cant_Be_Started_After_Remove_Two_Players()
        {
            for (var i = 0; i < 2; i++)
                _viewModel.RemovePlayerCommand.Execute(_viewModel.Players.First());

            Assert.IsFalse(_viewModel.StartChampionshipCommand.CanExecute(null));
        }

        [TestMethod]
        public void Championship_Cant_Be_Started_After_Remove_Ten_Players()
        {
            for (var i = 0; i < 10; i++)
                _viewModel.RemovePlayerCommand.Execute(_viewModel.Players.First());

            Assert.IsFalse(_viewModel.StartChampionshipCommand.CanExecute(null));
        }

        [TestMethod]
        public void Championship_Can_Be_Started()
        {
            for (var i = 0; i < 8; i++)
                _viewModel.RemovePlayerCommand.Execute(_viewModel.Players.First());

            Assert.IsTrue(_viewModel.StartChampionshipCommand.CanExecute(null));
        }

        [TestMethod]
        public void Players_Should_Be_Refreshed()
        {
            Assert.AreEqual(16, _viewModel.Players.Count);
            var player = _viewModel.Players.First();
            _viewModel.RemovePlayerCommand.Execute(player);
            Assert.AreEqual(15, _viewModel.Players.Count);

            _viewModel.RefreshPlayersCommand.Execute(null);
            Assert.AreEqual(16, _viewModel.Players.Count);
        }

        [TestMethod]
        public void Number_Of_Players_Required_Should_Be_8()
        {
            Assert.AreEqual(8, _viewModel.NumberOfPlayersRequired);
        }
    }
}
