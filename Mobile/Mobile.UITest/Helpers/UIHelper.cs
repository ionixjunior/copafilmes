﻿using Xamarin.UITest;

namespace Mobile.UITest.Helpers
{
    public class UIHelper
    {
        private readonly IApp _app;
        private readonly Platform _platform;

        public UIHelper(IApp app, Platform platform)
        {
            _app = app;
            _platform = platform;
        }

        public void ContextActionSelect(string element)
		{
            if (_platform == Platform.Android)
            {
                _app.TouchAndHold(element);
                return;
            }

            _app.SwipeRightToLeft(element);
		}
    }
}
