﻿using System;
using Mobile.UITest.Helpers;
using NUnit.Framework;
using Xamarin.UITest;

namespace Mobile.UITest
{
    [TestFixture(Platform.Android)]
    [TestFixture(Platform.iOS)]
    public class AppTest
    {
        private IApp _app;
        private Platform _platform;
        private UIHelper _uiHelper;

        public AppTest(Platform platform)
        {
            _platform = platform;
        }

        [SetUp]
        public void BeforeEachTest()
        {
            _app = AppInitializer.StartApp(_platform);
            _uiHelper = new UIHelper(_app, _platform);
        }

        [Test]
        public void Start_Championship_And_Show_Results()
        {
            var nameOfElementRemove = "Remover";

            _app.WaitForElement("Selecionados 16 de 8 Filmes");
            _app.Screenshot("Lista dos filmes completa");

            _uiHelper.ContextActionSelect("Os Incríveis 2");
            _app.Tap(nameOfElementRemove);

            _uiHelper.ContextActionSelect("Jurassic World: Reino Ameaçado");
            _app.Tap(nameOfElementRemove);

            _uiHelper.ContextActionSelect("Oito Mulheres e um Segredo");
            _app.Tap(nameOfElementRemove);

            _uiHelper.ContextActionSelect("Hereditário");
            _app.Tap(nameOfElementRemove);

            _uiHelper.ContextActionSelect("Vingadores: Guerra Infinita");
            _app.Tap(nameOfElementRemove);

            _uiHelper.ContextActionSelect("Deadpool 2");
            _app.Tap(nameOfElementRemove);

            _uiHelper.ContextActionSelect("Han Solo: Uma História Star Wars");
            _app.Tap(nameOfElementRemove);

            _uiHelper.ContextActionSelect("Thor: Ragnarok");
            _app.Tap(nameOfElementRemove);

            _app.WaitForElement("Selecionados 8 de 8 Filmes");
            _app.Screenshot("Lista dos filmes selecionados");

            _app.Tap("Gerar");
            _app.WaitForElement("1º");
            _app.WaitForElement("2º");
            _app.Screenshot("Lista dos resultados");
        }
    }
}
