﻿using System;
using System.Collections.ObjectModel;
using Api.Interfaces;
using Api.Models;

namespace Mobile.ViewModels
{
	public class ResultViewModel : ViewModelBase
    {
        private readonly IChampionshipService _championshipService;

        private ObservableCollection<Result> _results;
        public ObservableCollection<Result> Results
        {
            get { return _results; }
            set { SetProperty(ref _results, value); }
        }

        public ResultViewModel(IChampionshipService championshipService)
        {
            _championshipService = championshipService;

            Title = "Resultado";

            ShowResults();
        }

        private void ShowResults()
        {
            Results = new ObservableCollection<Result>();

            foreach (var result in _championshipService.GetResults())
                Results.Add(result);
        }
    }
}
