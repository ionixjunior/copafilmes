﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Api.Interfaces;
using Api.Models;
using Mobile.Views;
using Prism.Navigation;
using Prism.Services;
using Xamarin.Forms;

namespace Mobile.ViewModels
{
    public class PlayersViewModel : ViewModelBase
    {
        private readonly IChampionshipService _championshipService;
        private readonly IPageDialogService _dialogService;
        private readonly INavigationService _navigationService;

        private ObservableCollection<Movie> _players;
        public ObservableCollection<Movie> Players
        {
            get { return _players; }
            set { SetProperty(ref _players, value); }
        }

        private int _numberOfPlayersRequired;
        public int NumberOfPlayersRequired
        {
            get { return _numberOfPlayersRequired; }
            set { SetProperty(ref _numberOfPlayersRequired, value); }
        }

        public ICommand RemovePlayerCommand { get; private set; }
        public ICommand StartChampionshipCommand { get; private set; }
        public ICommand RefreshPlayersCommand { get; private set; }

        public PlayersViewModel(IChampionshipService championshipService,
                                IPageDialogService dialogService,
                                INavigationService navigationService)
        {
            _championshipService = championshipService;
            _dialogService = dialogService;
            _navigationService = navigationService;
           
            Title = "Fase de seleção";
            Initialization = InitializeAsync();

            RemovePlayerCommand = new Command<Movie>(RemovePlayer);

            StartChampionshipCommand = new Command(
                async () => await StartChampionship(), 
                () => ValidateNumberOfPlayers()
            );

            RefreshPlayersCommand = new Command(
                async () => await RefreshPlayersAsync()
            );
        }

        public Task Initialization { get; private set; }

        private async Task InitializeAsync()
        {
            if (IsBusy)
                return;

            try
            {
                IsBusy = true;
                Players = await _championshipService.GetPlayersAndStartSquadAsync();
                NumberOfPlayersRequired = _championshipService.GetNumberOfPlayersRequired();
            }
            catch (Exception exception)
            {
                await ShowExceptionMessageAsync(exception);
            }
            finally
            {
                IsBusy = false;
            }
        }

        private async Task ShowExceptionMessageAsync(Exception exception)
        {
            var title = "Ops, algo deu errado";
            var message = exception.Message;
            var button = "Ok";

            await _dialogService.DisplayAlertAsync(title, message, button);
        }

        private void RemovePlayer(Movie player)
        {
            _championshipService.RemovePlayerOfSquad(player);
            ((Command)StartChampionshipCommand).ChangeCanExecute();
        }

        private async Task StartChampionship()
        {
            _championshipService.StartChampionship();
            await _navigationService.NavigateAsync(typeof(ResultView).Name);
        }

        private bool ValidateNumberOfPlayers()
        {
            return _championshipService.ValidateNumberOfPlayers();
        }

        private async Task RefreshPlayersAsync()
        {
            Players?.Clear();
            await InitializeAsync();
        }
    }
}
