﻿using Xamarin.Forms;

namespace Mobile.Controls
{
    public class ListView : Xamarin.Forms.ListView
    {      
        public ListView() : base(ListViewCachingStrategy.RecycleElement)
        {
            ItemTapped += ListView_ItemTapped;
        }

        ~ListView()
        {
            ItemTapped -= ListView_ItemTapped;
        }

        private void ListView_ItemTapped(object sender, ItemTappedEventArgs e)
        {         
            SelectedItem = null;
        }
    }
}
