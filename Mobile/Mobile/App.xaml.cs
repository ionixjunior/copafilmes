﻿using Prism;
using Prism.Ioc;
using Mobile.ViewModels;
using Mobile.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Api.Interfaces;
using Api.Services;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace Mobile
{
    public partial class App
    {
        public App() : this(null) { }

        public App(IPlatformInitializer initializer) : base(initializer) { }

        protected override async void OnInitialized()
        {
            InitializeComponent();

            await NavigationService.NavigateAsync("NavigationPage/PlayersView");
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterForNavigation<NavigationPage>();
            containerRegistry.RegisterForNavigation<PlayersView, PlayersViewModel>();
            containerRegistry.RegisterForNavigation<ResultView, ResultViewModel>();

            containerRegistry.RegisterSingleton<IMovieService, MovieService>();
            containerRegistry.RegisterSingleton<IChampionshipService, ChampionshipService>();
        }
    }
}
