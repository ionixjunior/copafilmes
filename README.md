# Copa Filmes

Projeto desenvolvido com Xamarin.Forms para simular uma copa do mundo de filmes.

## Projetos

A solução possui sete projetos que estão divididos entre Api e Mobile.

### Projetos da Api
- Api
- Api.UnitTest

### Projetos do Mobile
- Mobile - projeto compartilhado
- Mobile.Android
- Mobile.iOS
- Mobile.UITest
- Mobile.UnitTest

## Iniciar o app

Para iniciar o app, abra o projeto do Visual Studio, compile o projeto e selecione a plataforma para fazer deploy.

Para o Android, o target framework é o 8.1

## Testes

### Unit test

Para executar os testes unitários basta executar na raiz do projeto:

Testes da Api:

```
$ dotnet test Api/Api.UnitTest/
```

Testes do Mobile:

```
$ dotnet test Mobile/Mobile.UnitTest/
```

### UI test

Os testes de UI foram realizados somente em modo Debug, sendo executado no iOS Simulator e em um dispositivo físico Android. Para executá-lo, utilize o Visual Studio. 

Boa sorte.

## Bibliotecas utilizadas no app
- Xamarin.Forms
- Prism.DryIoc.Forms
- Newtosoft.Json

## Observações

Este projeto não foi testado em modo Release, ou seja: podem existir problemas com o Linker.

