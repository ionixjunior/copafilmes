﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using Api.UnitTest.Fake;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Api.UnitTest.Services
{
    [TestClass]
    public abstract class HttpTestBase
    {
        public const string MOVIES_JSON_FILE = "movies.json";
        public const string WRONG_DATA_JSON_FILE = "wrong-data.json";

        public Mock<FakeHttpMessageHandler> Handler;
        public HttpClient Client;

        [TestInitialize]
        public void BaseInitialize()
        {
            Handler = new Mock<FakeHttpMessageHandler>() { CallBase = true };
            Client = new HttpClient(Handler.Object);
        }

        [TestCleanup]
        public void BaseCleanup()
        {
            Handler = null;
            Client = null;
        }

        public void MakeInternalServerErrorHttpResponse()
        {
            var httpResponseMessage = new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.InternalServerError
            };

            Handler.Setup(e => e.Send(It.IsAny<HttpRequestMessage>()))
                    .Returns(httpResponseMessage);
        }

        public void MakeOkHttpResponse(string filename)
        {
            var json = LoadSampleData(filename);
            var httpResponseMessage = new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(json)
            };

            Handler.Setup(e => e.Send(It.IsAny<HttpRequestMessage>()))
                    .Returns(httpResponseMessage);
        }

        public string LoadSampleData(string filename)
        {
            var assembly = GetType().Assembly;
            var stream = assembly.GetManifestResourceStream($"Api.UnitTest.Data.{filename}");

            var data = string.Empty;

            using (var reader = new StreamReader(stream))
                data = reader.ReadToEnd();

            return data;
        }
    }
}
