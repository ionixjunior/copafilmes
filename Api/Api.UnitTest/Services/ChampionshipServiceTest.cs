﻿using System.Linq;
using System.Collections.Generic;
using Api.Interfaces;
using Api.Models;
using Api.Services;
using Api.UnitTest.Fake;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using Api.Exceptions;

namespace Api.UnitTest.Services
{
    [TestClass]
    public class ChampionshipServiceTest : HttpTestBase
    {
        private MovieFake _movieFake;
        private IMovieService _movieService;
        private IChampionshipService _championshipService;

        [TestInitialize]
        public void Initialize()
        {
            _movieFake = MovieFake.Instance;
            _movieService = new MovieService(Client);
            _championshipService = new ChampionshipService(_movieService);
        }

        [TestCleanup]
        public void Cleanup()
        {
            _movieFake = null;
            Handler = null;
            Client = null;
            _movieService = null;
            _championshipService = null;
        }

        [TestMethod]
        public void Squad_Should_Be_Initialized()
        {
            Assert.IsNotNull(_championshipService.GetSquad());
        }

        [TestMethod]
        public async Task Selected_Players_Should_Be_Return()
        {
            MakeOkHttpResponse(MOVIES_JSON_FILE);

            await _championshipService.GetPlayersAndStartSquadAsync();

            Assert.AreEqual(16, _championshipService.GetSelectedPlayers().Count);
        }

        [TestMethod]
        public async Task Players_Should_Be_Ordered()
        {
            MakeOkHttpResponse(MOVIES_JSON_FILE);
            var players = await _championshipService.GetPlayersAndStartSquadAsync();
            RemoveNonStandardPlayersForTest(players);
            _championshipService.StartChampionship();

            var expected = _movieFake.GetOrderedMovies();
            var actual = _championshipService.GetOrderedPlayers();

            for (var i = 0; i < actual.Count; i++)
                Assert.AreEqual(expected[i].Name, actual[i].Name);
        }

        [TestMethod]
        public async Task Should_Be_Exists_Three_Rounds()
        {
            MakeOkHttpResponse(MOVIES_JSON_FILE);
            var players = await _championshipService.GetPlayersAndStartSquadAsync();
            RemoveNonStandardPlayersForTest(players);
            _championshipService.StartChampionship();

            var expected = _movieFake.GetRounds();
            var actual = _championshipService.GetRounds();

            Assert.AreEqual(expected.Count, actual.Count);
        }

        [TestMethod]
        public async Task Quarterfinals_Round_Should_Be_Contains_Fake_Movies()
        {
            MakeOkHttpResponse(MOVIES_JSON_FILE);
            var players = await _championshipService.GetPlayersAndStartSquadAsync();
            RemoveNonStandardPlayersForTest(players);
            _championshipService.StartChampionship();

            var round = _championshipService.GetRounds()[0];
            var matches = round.GetMatches();

            var actual = new List<Movie>();
            foreach (var match in matches)
            {
                var playersOfMatch = match.GetPlayers();
                playersOfMatch.ToList().ForEach(player => actual.Add(player));
            }

            Assert.AreEqual(8, actual.Count);
            Assert.IsTrue(actual.Contains(GetPlayerInSquad(_movieFake.Deadpool)));
            Assert.IsTrue(actual.Contains(GetPlayerInSquad(_movieFake.HanSolo)));
            Assert.IsTrue(actual.Contains(GetPlayerInSquad(_movieFake.Hereditario)));
            Assert.IsTrue(actual.Contains(GetPlayerInSquad(_movieFake.Jurassic)));
            Assert.IsTrue(actual.Contains(GetPlayerInSquad(_movieFake.OitoMulheres)));
            Assert.IsTrue(actual.Contains(GetPlayerInSquad(_movieFake.Incriveis2)));
            Assert.IsTrue(actual.Contains(GetPlayerInSquad(_movieFake.Thor)));
            Assert.IsTrue(actual.Contains(GetPlayerInSquad(_movieFake.Vingadores)));
        }

        [TestMethod]
        public async Task Semifinals_Round_Should_Be_Contains_Fake_Movies()
        {
            MakeOkHttpResponse(MOVIES_JSON_FILE);
            var players = await _championshipService.GetPlayersAndStartSquadAsync();
            RemoveNonStandardPlayersForTest(players);
            _championshipService.StartChampionship();

            var round = _championshipService.GetRounds()[1];
            var matches = round.GetMatches();

            var actual = new List<Movie>();
            foreach (var match in matches)
            {
                var playersOfMatch = match.GetPlayers();
                playersOfMatch.ToList().ForEach(player => actual.Add(player));
            }

            Assert.AreEqual(4, actual.Count);
            Assert.IsTrue(actual.Contains(GetPlayerInSquad(_movieFake.Vingadores)));
            Assert.IsTrue(actual.Contains(GetPlayerInSquad(_movieFake.Thor)));
            Assert.IsTrue(actual.Contains(GetPlayerInSquad(_movieFake.Incriveis2)));
            Assert.IsTrue(actual.Contains(GetPlayerInSquad(_movieFake.Jurassic)));
        }

        [TestMethod]
        public async Task Final_Round_Should_Be_Contains_Fake_Movies()
        {
            MakeOkHttpResponse(MOVIES_JSON_FILE);
            var players = await _championshipService.GetPlayersAndStartSquadAsync();
            RemoveNonStandardPlayersForTest(players);
            _championshipService.StartChampionship();

            var round = _championshipService.GetRounds()[2];
            var matches = round.GetMatches();

            var actual = new List<Movie>();
            foreach (var match in matches)
            {
                var playersOfMatch = match.GetPlayers();
                playersOfMatch.ToList().ForEach(player => actual.Add(player));
            }

            Assert.AreEqual(2, actual.Count);
            Assert.IsTrue(actual.Contains(GetPlayerInSquad(_movieFake.Vingadores)));
            Assert.IsTrue(actual.Contains(GetPlayerInSquad(_movieFake.Incriveis2)));
        }

        [TestMethod]
        public async Task Winner_Should_Be_Equals_Fake_Movie()
        {
            MakeOkHttpResponse(MOVIES_JSON_FILE);
            var players = await _championshipService.GetPlayersAndStartSquadAsync();
            RemoveNonStandardPlayersForTest(players);
            _championshipService.StartChampionship();

            var results = _championshipService.GetResults();
            var winner = results.First(e => e.Position == 1);

            Assert.AreEqual(GetPlayerInSquad(_movieFake.Vingadores), winner.Player);
        }

        [TestMethod]
        public async Task Looser_Should_Be_Equals_Fake_Movie()
        {
            MakeOkHttpResponse(MOVIES_JSON_FILE);
            var players = await _championshipService.GetPlayersAndStartSquadAsync();
            RemoveNonStandardPlayersForTest(players);
            _championshipService.StartChampionship();

            var results = _championshipService.GetResults();
            var winner = results.First(e => e.Position == 2);

            Assert.AreEqual(GetPlayerInSquad(_movieFake.Incriveis2), winner.Player);
        }

        [TestMethod]
        public async Task Get_Players_Should_Return_A_List_Of_Players()
        {
            MakeOkHttpResponse(MOVIES_JSON_FILE);
            var players = await _championshipService.GetPlayersAndStartSquadAsync();

            Assert.AreEqual(16, players.Count);
        }

        [TestMethod, ExpectedException(typeof(ApiException))]
        public async Task Get_Players_Should_Return_An_Exception()
        {
            MakeOkHttpResponse(WRONG_DATA_JSON_FILE);

            await _championshipService.GetPlayersAndStartSquadAsync();
        }

        [TestMethod, ExpectedException(typeof(ApiException))]
        public async Task Results_Of_Get_Players_That_Contains_Server_Error_Should_Return_An_Exception()
        {
            MakeInternalServerErrorHttpResponse();

            await _championshipService.GetPlayersAndStartSquadAsync();
        }

        [TestMethod]
        public async Task Player_Of_Squad_Should_Be_Removed()
        {
            MakeOkHttpResponse(MOVIES_JSON_FILE);
            var players = await _championshipService.GetPlayersAndStartSquadAsync();

            Assert.AreEqual(16, _championshipService.GetSquad().GetPlayers().Count);
            _championshipService.RemovePlayerOfSquad(players.First());
            Assert.AreEqual(15, _championshipService.GetSquad().GetPlayers().Count);
        }

        [TestMethod]
        public async Task Number_Of_Players_On_Squad_Should_Be_Invalid()
        {
            MakeOkHttpResponse(MOVIES_JSON_FILE);
            var players = await _championshipService.GetPlayersAndStartSquadAsync();

            Assert.IsFalse(_championshipService.ValidateNumberOfPlayers());
        }

        [TestMethod]
        public async Task Number_Of_Players_On_Squad_Should_Be_Valid()
        {
            MakeOkHttpResponse(MOVIES_JSON_FILE);

            var players = await _championshipService.GetPlayersAndStartSquadAsync();
            RemoveNonStandardPlayersForTest(players);

            Assert.IsTrue(_championshipService.ValidateNumberOfPlayers());
        }

        [TestMethod]
        public void Number_Of_Players_Required_Should_Be_8()
        {
            Assert.AreEqual(8, _championshipService.GetNumberOfPlayersRequired());
        }

        [TestMethod]
        public async Task Restart_Championship_Should_Be_Restart_Rounds()
        {
            MakeOkHttpResponse(MOVIES_JSON_FILE);

            var players1 = await _championshipService.GetPlayersAndStartSquadAsync();
            RemoveNonStandardPlayersForTest(players1);
            _championshipService.StartChampionship();

            Assert.AreEqual(3, _championshipService.GetRounds().Count);

            var players2 = await _championshipService.GetPlayersAndStartSquadAsync();
            RemoveNonStandardPlayersForTest(players2);
            _championshipService.StartChampionship();

            Assert.AreEqual(3, _championshipService.GetRounds().Count);
        }

        private void RemoveNonStandardPlayersForTest(IList<Movie> players)
        {
            var playersThatShouldBeRemoved = new List<Movie>
            {
                _movieFake.TePeguei,
                _movieFake.Incriveis,
                _movieFake.BarracaBeijo,
                _movieFake.TombRaider,
                _movieFake.Pantera,
                _movieFake.Hotel,
                _movieFake.Superfly,
                _movieFake.Upgrade
            };

            foreach (var player in playersThatShouldBeRemoved)
            {
                var playerInSquad = GetPlayerInSquad(player);

                _championshipService.RemovePlayerOfSquad(playerInSquad);
            }
        }

        private Movie GetPlayerInSquad(Movie player)
        {
            return _championshipService.GetSquad()
                                       .GetPlayers()
                                       .First(p => p.Id == player.Id);
        }
    }
}
