﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Api.Exceptions;
using Api.Interfaces;
using Api.Services;
using Api.UnitTest.Fake;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Api.UnitTest.Services
{
    [TestClass]
    public class MovieServiceTest
    {
        private Mock<FakeHttpMessageHandler> _handler;
        private HttpClient _client;
        private IMovieService _movieService;

        [TestInitialize]
        public void Initialize()
        {
            _handler = new Mock<FakeHttpMessageHandler>() { CallBase = true };
            _client = new HttpClient(_handler.Object);
            _movieService = new MovieService(_client);
        }

        [TestCleanup]
        public void Cleanup()
        {
            _handler = null;
            _client = null;
            _movieService = null;
        }

        [TestMethod]
        public async Task Service_Should_Return_A_List_Of_Movies()
        {
            var json = "[{\"id\":\"1\",\"titulo\":\"Name 1\"},{\"id\":\"2\",\"titulo\":\"Name 2\"}]";
            var httpResponseMessage = new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.OK, 
                Content = new StringContent(json)
            };

            _handler.Setup(e => e.Send(It.IsAny<HttpRequestMessage>()))
                    .Returns(httpResponseMessage);

            var movies = await _movieService.GetMoviesAsync();
            Assert.AreEqual(2, movies.Count);
        }

        [TestMethod, ExpectedException(typeof(ApiException))]
        public async Task Service_Should_Return_An_Exception()
        {
            var json = "{}";
            var httpResponseMessage = new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(json)
            };

            _handler.Setup(e => e.Send(It.IsAny<HttpRequestMessage>()))
                    .Returns(httpResponseMessage);

            await _movieService.GetMoviesAsync();
        }
    }
}
