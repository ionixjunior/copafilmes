﻿using System;
using Api.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Api.UnitTest.Models
{
    [TestClass]
    public class ChampionshipTest
    {
        private Championship _championship;

        [TestInitialize]
        public void Initialize()
        {
            _championship = new Championship();
        }

        [TestCleanup]
        public void Cleanup()
        {
            _championship = null;
        }

        [TestMethod]
        public void Rounds_Should_Be_Initialized_When_New_Instance()
        {
            Assert.AreEqual(0, _championship.GetRounds().Count);
        }

        [TestMethod]
        public void Should_Be_Possible_Add_Rounds()
        {
            _championship.AddRound(new Round());
            Assert.AreEqual(1, _championship.GetRounds().Count);
        }
    }
}
