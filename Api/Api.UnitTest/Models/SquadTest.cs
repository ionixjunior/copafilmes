﻿using System.Linq;
using System.Collections.ObjectModel;
using Api.Models;
using Api.UnitTest.Fake;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Api.UnitTest.Models
{
    [TestClass]
    public class SquadTest
    {
        private MovieFake _movieFake;
        private Squad _squad;

        [TestInitialize]
        public void Initialize()
        {
            _movieFake = MovieFake.Instance;
            _squad = new Squad();
        }

        [TestCleanup]
        public void Cleanup()
        {
            _movieFake = null;
            _squad = null;
        }

        [TestMethod]
        public void Number_Of_Players_Required_Should_Be_8()
        {
            Assert.AreEqual(8, _squad.GetNumberOfPlayersRequired());
        }

        [TestMethod]
        public void Players_Should_Be_An_Instance_Of_ObservableCollection()
        {
            Assert.IsInstanceOfType(_squad.GetPlayers(), typeof(ObservableCollection<Movie>));
        }

        [TestMethod]
        public void Players_Should_Be_Removed()
        {
            _squad.Start(_movieFake.GetAllMovies());
            var players = _squad.GetPlayers();

            Assert.AreEqual(16, players.Count);

            _squad.RemovePlayer(players.First());
            _squad.RemovePlayer(players.Last());

            Assert.AreEqual(14, players.Count);
        }

        [TestMethod]
        public void Number_Of_Players_Verification_Should_Be_Invalid()
        {
            _squad.Start(_movieFake.GetAllMovies());
            Assert.IsFalse(_squad.ValidateNumberOfPlayers());
        }

        [TestMethod]
        public void Number_Of_Players_Verification_Should_Be_Valid()
        {
            _squad.Start(_movieFake.GetAllMovies());

            _squad.RemovePlayer(_movieFake.BarracaBeijo);
            _squad.RemovePlayer(_movieFake.Deadpool);
            _squad.RemovePlayer(_movieFake.HanSolo);
            _squad.RemovePlayer(_movieFake.Hereditario);
            _squad.RemovePlayer(_movieFake.Hotel);
            _squad.RemovePlayer(_movieFake.Incriveis);
            _squad.RemovePlayer(_movieFake.Incriveis2);
            _squad.RemovePlayer(_movieFake.Jurassic);

            Assert.IsTrue(_squad.ValidateNumberOfPlayers());
        }

        [TestMethod]
        public void Players_Should_Be_Reseted()
        {
            _squad.Start(_movieFake.GetAllMovies());

            _squad.RemovePlayer(_movieFake.BarracaBeijo);
            _squad.RemovePlayer(_movieFake.Deadpool);

            Assert.AreEqual(14, _squad.GetPlayers().Count);

            _squad.Start(_movieFake.GetAllMovies());

            Assert.AreEqual(16, _squad.GetPlayers().Count);
            Assert.IsTrue(_squad.GetPlayers().Contains(_movieFake.BarracaBeijo));
            Assert.IsTrue(_squad.GetPlayers().Contains(_movieFake.Deadpool));
        }
    }
}
