﻿using System;
using Api.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Api.UnitTest.Models
{
    [TestClass]
    public class RoundTest
    {
        private Round _round;

        [TestInitialize]
        public void Initialize()
        {
            _round = new Round();
        }

        [TestCleanup]
        public void Cleanup()
        {
            _round = null;
        }

        [TestMethod]
        public void Matches_Should_Be_Initialized_When_New_Instance()
        {
            Assert.AreEqual(0, _round.GetMatches().Count);
        }

        [TestMethod]
        public void Should_Be_Possible_Add_Matches()
        {
            _round.AddMatch(new Match(new Movie(), new Movie()));
            Assert.AreEqual(1, _round.GetMatches().Count);
        }

        [TestMethod]
        public void Movie1_And_Movie4_Should_Be_Winners()
        {
            var movie1 = new Movie() { Name = "Movie 1", Rating = 8.8 };
            var movie2 = new Movie() { Name = "Movie 2", Rating = 7.5 };
            var movie3 = new Movie() { Name = "Movie 3", Rating = 8.9 };
            var movie4 = new Movie() { Name = "Movie 4", Rating = 9.1 };

            _round.AddMatch(new Match(movie1, movie2));
            _round.AddMatch(new Match(movie3, movie4));

            _round.PlayMatches();
            var winners = _round.GetWinners();

            Assert.IsTrue(winners.Contains(movie1));
            Assert.IsTrue(winners.Contains(movie4));
            Assert.IsFalse(winners.Contains(movie2));
            Assert.IsFalse(winners.Contains(movie3));
        }
    }
}
