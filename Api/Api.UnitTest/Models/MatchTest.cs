﻿using System;
using Api.Exceptions;
using Api.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Api.UnitTest.Models
{
    [TestClass]
    public class MatchTest
    {
        private Movie _movie1;
        private Movie _movie2;

        [TestInitialize]
        public void Initialize()
        {
            _movie1 = new Movie();
            _movie2 = new Movie();
        }

        [TestCleanup]
        public void Cleanup()
        {
            _movie1 = null;
            _movie2 = null;
        }

        [TestMethod, ExpectedException(typeof(ApiException))]
        public void Should_Throw_An_Exception_If_Movie_1_Is_Null()
        {
            _movie1 = null;
            var match = new Match(_movie1, _movie1);
        }

        [TestMethod]
        public void Movie1_Should_Be_Winner_By_Rating()
        {
            _movie1 = new Movie() { Name = "Movie 1", Rating = 9.3 };
            _movie2 = new Movie() { Name = "Movie 2", Rating = 8.7 };

            var match = new Match(_movie1, _movie2);
            match.Play();

            Assert.AreEqual(_movie1, match.GetWinner());
        }

        [TestMethod]
        public void Movie2_Should_Be_Winner_By_Rating()
        {
            _movie1 = new Movie() { Name = "Movie 1", Rating = 9.3 };
            _movie2 = new Movie() { Name = "Movie 2", Rating = 9.5 };

            var match = new Match(_movie1, _movie2);
            match.Play();

            Assert.AreEqual(_movie2, match.GetWinner());
        }

        [TestMethod]
        public void Movie1_Should_Be_Winner_By_Order_Name()
        {
            _movie1 = new Movie() { Name = "Movie 1", Rating = 9.5 };
            _movie2 = new Movie() { Name = "Movie 2", Rating = 9.5 };

            var match = new Match(_movie1, _movie2);
            match.Play();

            Assert.AreEqual(_movie1, match.GetWinner());
        }
    }
}
