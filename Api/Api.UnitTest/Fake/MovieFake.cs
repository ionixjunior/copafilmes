﻿using System;
using System.Collections.Generic;
using Api.Models;

namespace Api.UnitTest.Fake
{
    public class MovieFake
    {
        private static MovieFake _instance;
        public static MovieFake Instance => _instance ?? (_instance = new MovieFake());

        public Movie Incriveis2;
        public Movie Jurassic;
        public Movie OitoMulheres;
        public Movie Hereditario;
        public Movie Vingadores;
        public Movie Deadpool;
        public Movie HanSolo;
        public Movie Thor;
        public Movie TePeguei;
        public Movie Incriveis;
        public Movie BarracaBeijo;
        public Movie TombRaider;
        public Movie Pantera;
        public Movie Hotel;
        public Movie Superfly;
        public Movie Upgrade;

        private IList<Movie> _selectedMovies;
        private IList<Movie> _allMovies;

        private MovieFake()
        {
            Incriveis2 = GetMovie("tt3606756", "Os Incríveis 2", 2018, 8.5);
            Jurassic = GetMovie("tt4881806", "Jurassic World: Reino Ameaçado", 2018, 6.7);
            OitoMulheres = GetMovie("tt5164214", "Oito Mulheres e um Segredo", 2018, 6.3);
            Hereditario = GetMovie("tt7784604", "Hereditário", 2018, 7.8);
            Vingadores = GetMovie("tt4154756", "Vingadores: Guerra Infinita", 2018, 8.8);
            Deadpool = GetMovie("tt5463162", "Deadpool 2", 2018, 8.1);
            HanSolo = GetMovie("tt3778644", "Han Solo: Uma História Star Wars", 2018, 7.2);
            Thor = GetMovie("tt3501632", "Thor: Ragnarok", 2017, 7.9);
            TePeguei = GetMovie("tt2854926", "Te Peguei!", 2018, 7.1);
            Incriveis = GetMovie("tt0317705", "Os Incríveis", 2004, 8);
            BarracaBeijo = GetMovie("tt3799232", "A Barraca do Beijo", 2018, 6.4);
            TombRaider = GetMovie("tt1365519", "Tomb Raider: A Origem", 2018, 6.5);
            Pantera = GetMovie("tt1825683", "Pantera Negra", 2018, 7.5);
            Hotel = GetMovie("tt5834262", "Hotel Artemis", 2018, 6.3);
            Superfly = GetMovie("tt7690670", "Superfly", 2018, 5.1);
            Upgrade = GetMovie("tt6499752", "Upgrade", 2018, 7.8);
        }

        private Movie GetMovie(string id, string name, int releaseYear, double rating)
        {
            return new Movie()
            {
                Id = id,
                Name = name,
                ReleaseYear = releaseYear,
                Rating = rating
            };
        }

        public IList<Movie> GetSelectedMovies()
        {
            if (_selectedMovies == null)
            {
                _selectedMovies = new List<Movie>();

                _selectedMovies.Add(Incriveis2);
                _selectedMovies.Add(Jurassic);
                _selectedMovies.Add(OitoMulheres);
                _selectedMovies.Add(Hereditario);
                _selectedMovies.Add(Vingadores);
                _selectedMovies.Add(Deadpool);
                _selectedMovies.Add(HanSolo);
                _selectedMovies.Add(Thor);
            }

            return _selectedMovies;
        }

        public IList<Movie> GetOrderedMovies()
        {
            var movies = new List<Movie>();

            movies.Add(Deadpool);
            movies.Add(HanSolo);
            movies.Add(Hereditario);
            movies.Add(Jurassic);
            movies.Add(OitoMulheres);
            movies.Add(Incriveis2);
            movies.Add(Thor);
            movies.Add(Vingadores);

            return movies;
        }

        public IList<Round> GetRounds()
        {
            var rounds = new List<Round>();

            var round1 = new Round();
            round1.AddMatch(new Match(Deadpool, Vingadores));
            round1.AddMatch(new Match(HanSolo, Thor));
            round1.AddMatch(new Match(Hereditario, Incriveis2));
            round1.AddMatch(new Match(Jurassic, OitoMulheres));
            rounds.Add(round1);

            var round2 = new Round();
            round2.AddMatch(new Match(Vingadores, Thor));
            round2.AddMatch(new Match(Incriveis2, Jurassic));
            rounds.Add(round2);

            var round3 = new Round();
            round3.AddMatch(new Match(Vingadores, Incriveis2));
            rounds.Add(round3);

            return rounds;
        }

        public IList<Movie> GetAllMovies()
        {
            if (_allMovies == null)
            {
                _allMovies = new List<Movie>();

                _allMovies.Add(Incriveis2);
                _allMovies.Add(Jurassic);
                _allMovies.Add(OitoMulheres);
                _allMovies.Add(Hereditario);
                _allMovies.Add(Vingadores);
                _allMovies.Add(Deadpool);
                _allMovies.Add(HanSolo);
                _allMovies.Add(Thor);
                _allMovies.Add(TePeguei);
                _allMovies.Add(Incriveis);
                _allMovies.Add(BarracaBeijo);
                _allMovies.Add(TombRaider);
                _allMovies.Add(Pantera);
                _allMovies.Add(Hotel);
                _allMovies.Add(Superfly);
                _allMovies.Add(Upgrade);
            }

            return _allMovies;
        }
    }
}
