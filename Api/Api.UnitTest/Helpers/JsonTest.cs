﻿using System.Collections.Generic;
using Api.Exceptions;
using Api.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Api.UnitTest.Helpers
{
    [TestClass]
    public class JsonTest
    {
        [TestMethod]
        public void Json_Data_Should_Be_Deserialized_Successfully()
        {
            var json = "[{\"id\":\"1\",\"name\":\"Name 1\"},{\"id\":\"2\",\"name\":\"Name 2\"}]";
            var samples = Json.DeserializeObject<IList<Sample>>(json);

            Assert.AreEqual(2, samples.Count);
        }

        [TestMethod, ExpectedException(typeof(ApiException))]
        public void Unexpected_Data_Should_Throw_An_Exception()
        {
            var invalidData = "{}";
            var sample = Json.DeserializeObject<IList<Sample>>(invalidData);
        }

        private class Sample
        {
            public string Id { get; set; }
            public string Name { get; set; }
        }
    }
}
