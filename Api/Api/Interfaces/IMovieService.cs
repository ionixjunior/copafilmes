﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Api.Models;

namespace Api.Interfaces
{
    public interface IMovieService
    {
        Task<IList<Movie>> GetMoviesAsync();
    }
}
