﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Api.Models;

namespace Api.Interfaces
{
    public interface IChampionshipService
    {
        Task<ObservableCollection<Movie>> GetPlayersAndStartSquadAsync();
        void RemovePlayerOfSquad(Movie player);
        bool ValidateNumberOfPlayers();
        void StartChampionship();
        IList<Result> GetResults();
        int GetNumberOfPlayersRequired();

        IList<Movie> GetSelectedPlayers();
        IList<Movie> GetOrderedPlayers();
        IList<Round> GetRounds();
        Squad GetSquad();
    }
}
