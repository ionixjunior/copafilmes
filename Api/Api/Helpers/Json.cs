﻿using System;
using Api.Exceptions;
using Newtonsoft.Json;

namespace Api.Helpers
{
    public static class Json
    {
        public static TObject DeserializeObject<TObject>(string data) where TObject : class
        {
            try
            {
                return JsonConvert.DeserializeObject<TObject>(data);
            }
            catch (Exception exception)
            {
                throw new ApiException("O formato das informações é inválido.", exception);
            }
        }
    }
}
