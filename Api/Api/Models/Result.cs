﻿namespace Api.Models
{
    public class Result
    {
        public int Position { get; set; }
        public Movie Player { get; set; }
    }
}
