﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Api.Models
{
    public class Squad
    {
        private int _numberOfPlayersRequired;
        private ObservableCollection<Movie> _players;

        public Squad()
        {
            _numberOfPlayersRequired = 8;
            _players = new ObservableCollection<Movie>();
        }

        public void Start(IList<Movie> players)
        {
            _players.Clear();

            foreach (var player in players)
                _players.Add(player);
        }

        public int GetNumberOfPlayersRequired()
        {
            return _numberOfPlayersRequired;
        }

        public ObservableCollection<Movie> GetPlayers()
        {
            return _players;
        }

        public void RemovePlayer(Movie player)
        {
            _players.Remove(player);
        }

        public bool ValidateNumberOfPlayers()
        {
            if (_players.Count.Equals(_numberOfPlayersRequired))
                return true;

            return false;
        }
    }
}
