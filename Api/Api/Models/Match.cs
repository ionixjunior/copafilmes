﻿using System.Linq;
using System.Collections.Generic;
using Api.Exceptions;

namespace Api.Models
{
    public class Match
    {
        private const int GREATER = 1;
        private const int EQUALS = 0;

        private IList<Movie> _players;
        private Movie _winner;

        public Match(Movie movie1, Movie movie2)
        {
            _players = new List<Movie>();
            _players.Add(movie1);
            _players.Add(movie2);

            if (CheckIfPlayersAreInvalid())
                throw new ApiException("Não foi possível criar a partida pois um dos adversários é inválido.");
        }

        private bool CheckIfPlayersAreInvalid()
        {
            if (_players.First() == null || _players.Last() == null)
                return true;

            return false;
        }

        public void Play()
        {
            CheckPlayerThatHasGreaterRating();
        }

        private void CheckPlayerThatHasGreaterRating()
        {
            if (CompareIfRatingIsEquals())
            {
                _winner = GetWinnerByOrderedName();
                return;
            }

            if (CompareIfFirstPlayerHasGreaterRating())
            {
                _winner = _players.First();
                return;
            }

            _winner = _players.Last();
        }

        private bool CompareIfRatingIsEquals()
        {
            if (_players.First().Rating.CompareTo(_players.Last().Rating).Equals(EQUALS))
                return true;

            return false;
        }

        private Movie GetWinnerByOrderedName()
        {
            return _players.OrderBy(p => p.Name)
                           .First();
        }

        private bool CompareIfFirstPlayerHasGreaterRating()
        {
            if (_players.First().Rating.CompareTo(_players.Last().Rating).Equals(GREATER))
                return true;

            return false;
        }

        public Movie GetWinner()
        {
            return _winner;
        }

        public Movie GetLooser()
        {
            return _players.First(p => p.Id != _winner.Id);
        }

        public IList<Movie> GetPlayers()
        {
            return _players;
        }
    }
}
