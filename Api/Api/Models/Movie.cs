﻿using Newtonsoft.Json;

namespace Api.Models
{
    public class Movie
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("titulo")]
        public string Name { get; set; }

        [JsonProperty("ano")]
        public int ReleaseYear { get; set; }

        [JsonProperty("nota")]
        public double Rating { get; set; }
    }
}
