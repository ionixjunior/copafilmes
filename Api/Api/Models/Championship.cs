﻿using System;
using System.Collections.Generic;

namespace Api.Models
{
    public class Championship
    {
        private IList<Round> _rounds;

        public Championship()
        {
            _rounds = new List<Round>();
        }

        public IList<Round> GetRounds()
        {
            return _rounds;
        }

        public void AddRound(Round round)
        {
            _rounds.Add(round);
        }

        public void Clear()
        {
            _rounds.Clear();
        }
    }
}
