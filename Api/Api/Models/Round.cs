﻿using System;
using System.Collections.Generic;

namespace Api.Models
{
    public class Round
    {
        private IList<Match> _matches;

        public Round()
        {
            _matches = new List<Match>();
        }

        public IList<Match> GetMatches()
        {
            return _matches;
        }

        public void AddMatch(Match match)
        {
            _matches.Add(match);
        }

        public void PlayMatches()
        {
            foreach (var match in _matches)
                match.Play();
        }

        public IList<Movie> GetWinners()
        {
            var winners = new List<Movie>();

            foreach (var match in _matches)
                winners.Add(match.GetWinner());

            return winners;
        }
    }
}
