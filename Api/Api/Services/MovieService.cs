﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Api.Exceptions;
using Api.Helpers;
using Api.Interfaces;
using Api.Models;

namespace Api.Services
{
    public class MovieService : IMovieService
    {
        private readonly HttpClient _client;
        private readonly Uri _uri;

        public MovieService() : this(new HttpClient()) 
        {
        }

        public MovieService(HttpClient client)
        {
            _client = client;
            _uri = new Uri("http://copafilmes.azurewebsites.net/api/filmes");
        }

        public async Task<IList<Movie>> GetMoviesAsync()
        {
            IList<Movie> movies = null;

            try
            {
                var response = await _client.GetAsync(_uri);
                response.EnsureSuccessStatusCode();

                var content = await response.Content.ReadAsStringAsync();
                movies = Json.DeserializeObject<IList<Movie>>(content);
            }
            catch (HttpRequestException exception)
            {
                throw new ApiException("Não foi possível conectar-se ao serviço. Aguarde alguns instantes e tente novamente.", exception);
            }
            catch (ApiException exception)
            {
                throw exception;
            }
            catch (Exception exception)
            {
                throw new ApiException("Não foi possível carregar a lista dos filmes.", exception);
            }

            return movies;
        }
    }
}
