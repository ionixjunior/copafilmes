﻿using System.Linq;
using System.Collections.Generic;
using Api.Interfaces;
using Api.Models;
using System.Threading.Tasks;
using System;
using System.Collections.ObjectModel;

namespace Api.Services
{
    public class ChampionshipService : IChampionshipService
    {
        private readonly IMovieService _movieService;

        private Squad _squad;
        private Championship _championship;
        private IList<Movie> _orderedPlayers;
        private IList<Movie> _finalistPlayers;
        private int _lastRound = 0;

        public ChampionshipService(IMovieService movieService)
        {
            _movieService = movieService;
            _squad = new Squad();
            _championship = new Championship();
        }

        public virtual async Task<ObservableCollection<Movie>> GetPlayersAndStartSquadAsync()
        {
            var players = await _movieService.GetMoviesAsync();

            _squad.Start(players);

            return _squad.GetPlayers();
        }

        public void RemovePlayerOfSquad(Movie player)
        {
            _squad.RemovePlayer(player);
        }

        public bool ValidateNumberOfPlayers()
        {
            return _squad.ValidateNumberOfPlayers();
        }

        public void StartChampionship()
        {
            ClearChampionship();
            SortByName();
            MakeFirstFinalists();
            PrepareRound();
            PlayMatches();
        }

        private void ClearChampionship()
        {
            _championship.Clear();
            _lastRound = 0;
        }

        private void SortByName()
        {
            _orderedPlayers = _squad.GetPlayers()
                                    .OrderBy(e => e.Name)
                                    .ToList();
        }

        private void MakeFirstFinalists()
        {
            var copyOfOrderedPlayers = _orderedPlayers.ToList();
            _finalistPlayers = new List<Movie>();

            while (copyOfOrderedPlayers.Count > 0)
            {
                _finalistPlayers.Add(MakeFirstFinalistsFirst(copyOfOrderedPlayers));
                _finalistPlayers.Add(MakeFirstFinalistsLast(copyOfOrderedPlayers));
            }
        }

        private Movie MakeFirstFinalistsFirst(IList<Movie> players)
        {
            var player = players.First();
            players.Remove(player);
            return player;
        }

        private Movie MakeFirstFinalistsLast(IList<Movie> players)
        {
            var player = players.Last();
            players.Remove(player);
            return player;
        }

        private void PrepareRound()
        {
            var totalPlayers = _finalistPlayers.Count;
            var index = 0;
            var round = new Round();

            while (index < totalPlayers)
            {
                var player1 = _finalistPlayers[index++];
                var player2 = _finalistPlayers[index++];

                var match = new Match(player1, player2);
                round.AddMatch(match);
            }

            _championship.AddRound(round);
        }

        private void PlayMatches()
        {
            var currentRound = _championship.GetRounds()
                                            .Skip(_lastRound++)
                                            .First();

            currentRound.PlayMatches();
            var winners = currentRound.GetWinners();
            AddWinnersToFinilistList(winners);

            if (CheckIsNecessaryMoreRounds())
            {
                PrepareRound();
                PlayMatches();
            }
        }

        private bool CheckIsNecessaryMoreRounds()
        {
            if (_finalistPlayers.Count == 1)
                return false;

            return true;
        }

        private void AddWinnersToFinilistList(IList<Movie> winners)
        {
            _finalistPlayers.Clear();

            foreach (var winner in winners)
                _finalistPlayers.Add(winner);
        }

        public IList<Movie> GetSelectedPlayers()
        {
            return _squad.GetPlayers();
        }

        public IList<Movie> GetOrderedPlayers()
        {
            return _orderedPlayers;
        }

        public IList<Round> GetRounds()
        {
            return _championship.GetRounds();
        }

        public virtual IList<Result> GetResults()
        {
            var results = new List<Result>();

            var lastRound = GetRounds().Last();
            var match = lastRound.GetMatches().First();

            results.Add(new Result() { Position = 1, Player = match.GetWinner() });
            results.Add(new Result() { Position = 2, Player = match.GetLooser() });

            return results;
        }

        public Squad GetSquad()
        {
            return _squad;
        }

        public int GetNumberOfPlayersRequired()
        {
            return _squad.GetNumberOfPlayersRequired();
        }
    }
}
